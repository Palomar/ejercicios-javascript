A partir d'un full html en blanc, crea, m�nim, un element dins la teva p�gina web, centra'l amb FlexBox, dona-li estil amb css i crea dos botons (Apareixer i Desapareixer) amb els que far�s de mag fent apareixer i desapareixer elements del DOM.
Amb un parell de botons m�s fes que l'element canvi� a un parell de colors.
Lliura els fitxers de codi en un arxiu comprimit.
XXL: Fes que l'usuari pugui triar els colors des d'una paleta de colors i que pugui restaurar el color original.